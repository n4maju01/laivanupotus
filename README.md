# Laivanupotus #

http://pitkamatkaeipelota.fi/laivanupotus/

### FI ###

Yksinkertainen PHP:llä toteutettu laivanupotuspeli. Syötä koordinaatit (0, 0) - (4, 4), ammu ja yritä upottaa kaikki laivat. Laivat ovat yhden ruudun kokoisia ja niitä on 5 kpl.

### EN ###

This is a simple battleships game written in PHP. Enter coordinates (0, 0) - (4, 4), shoot and try to sink all the boats. There are 5 one-square ships.

The interface is in Finnish.