<?php
//Tuhotaan istuntomuuttujat ja istunto ennen uuden pelin aloittamista
session_start();
unset($_SESSION["osumatjm"]);
unset($_SESSION["laukauksiajm"]);
unset($_SESSION["pelialoitettujm"]);
unset($_SESSION["laivatjm"]);
unset($_SESSION["pelilautajm"]);
session_destroy();
header('Location: index.php');
?>