<?php 
    session_start();
    session_regenerate_id();
?>

<html>
    <head>
        <meta charset="UTF-8">
        <meta name="author" content="Jussi Mannermaa">
        <title>Laivanupotus</title>
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    <body>
        <h3>Laivanupotuspeli</h3>
        <?php
        //Vakiot pelilaudan merkeille ja koolle
        define("KOORDINAATTIJM", "O");
        define("LAIVAJM", "X");
        define("HUTIKUTIJM", "-");
        define("RIVEJAJM", 5);
        define("SARAKKEITAJM", 5);
        define("LAIVOJAJM", 5);
   
        //Tarkastetaan, onko peli aloitettu.
        if (!isset($_SESSION["pelialoitettujm"])) {
            $_SESSION["pelialoitettujm"]=true;
            $_SESSION["osumatjm"]=0;
            $_SESSION["laukauksiajm"]=0;
            $arvottujajm = 0;
            
            //Määritellään kaksiulotteinen taulukko pelilaudalle ja laivojen paikoille.
            $pelilautajm=array(array());
            $laivatjm=array(array());
            
            //Alustetaan pelilauta, eli asetetaan koordinaatin merkki joka soluun.
            for ($i=0; $i<SARAKKEITAJM; $i++) {
                for ($j=0; $j<RIVEJAJM; $j++) {
                    $pelilautajm[$i][$j]=KOORDINAATTIJM;
                    $laivatjm[$i][$j]=KOORDINAATTIJM;
                }
            }
        
            //Arvotaan laudalle laivat.
            while ($arvottujajm<LAIVOJAJM) {
                $xjm=rand(0, SARAKKEITAJM-1);
                $yjm=rand(0, RIVEJAJM-1);
            
                //Tarkastetaan, että kohdassa ei ole vielä laivaa.
                if ($laivatjm[$xjm][$yjm] != LAIVAJM) {
                    $laivatjm[$xjm][$yjm] = LAIVAJM;
                    $arvottujajm++;
                }
            }
            
            $_SESSION["pelilautajm"]=$pelilautajm;
            $_SESSION["laivatjm"]=$laivatjm;
        }
        
        //Jos peli on aloitettu, tarkastellaan ammunnan tulos.
        else if (isset($_SESSION["pelialoitettujm"]) && 
                $_SERVER["REQUEST_METHOD"]=="POST") {
            $xjm=$_POST["txtXjm"];
            $yjm=$_POST["txtYjm"];
            $_SESSION["laukauksiajm"]++;
            
            if ($xjm<SARAKKEITAJM && $yjm<RIVEJAJM) {
                if ($_SESSION["pelilautajm"][$xjm][$yjm]==KOORDINAATTIJM &&
                        $_SESSION["laivatjm"][$xjm][$yjm]==LAIVAJM) {
                    $_SESSION["osumatjm"]++;
                    $_SESSION["pelilautajm"][$xjm][$yjm]=LAIVAJM;
                    print "Osuit!<br>";
                }
                
                else if ($_SESSION["pelilautajm"][$xjm][$yjm]==LAIVAJM) {
                    print "Olet jo ampunut tämän laivan!<br>";
                }
                
                else if ($_SESSION["pelilautajm"][$xjm][$yjm]==HUTIKUTIJM) {
                    print "Tässä ei edelleenkään ole laivaa!<br>";
                }
                
                else {
                    $_SESSION["pelilautajm"][$xjm][$yjm]=HUTIKUTIJM;
                    print "Ohi meni!<br>";
                }
            }
            
            else {
                print "Ammuit ohi pelilaudalta, yritä uudestaan!<br>";
            }
        }
        
        //Tulostetaan pelilauta HTML-taulukkona.
        print "<table>";
        for ($i=0; $i<RIVEJAJM; $i++) {
            print "<tr>";
            for ($j=0; $j<SARAKKEITAJM; $j++) {
                print "<td>" . $_SESSION["pelilautajm"][$j][$i] . "</td>";
            }
            print "</tr>";
        }
        print "</table>";
        ?>
        <?php
        //Tulostetaan lomake vain siinä tapauksessa, jos peli ei ole päättynyt.
        if ($_SESSION["osumatjm"]<LAIVOJAJM) {
        ?> 
            <br>
            <form action="<?php print $_SERVER['PHP_SELF'];?>" method="POST">
                X:<input name="txtXjm" size="1" maxlength="1">&nbsp;
                Y:<input name="txtYjm" size="1" maxlength="1">
                <input type="submit" value="Ammu">
            </form>
            <a href="uusipeli.php">Uusi peli</a>
        <?php
        }
        //Peli on päättynyt, koska osumia ei ole vähemmän kuin laivoja.
        else {
            print "<br>Kaikki laivat upotettu!<br>";
            print "<a href='uusipeli.php'>Uusi peli</a><br>";
        }
        ?>
    </body>
</html>